# Change Log
All notable changes to Flickr-Rust project will be documented in this file.

The format is based on [Keep a Changelog] and this project adheres to [Semantic Versioning].

## 0.1.1 - 2018-12-04
### Added
- Added support for flickr.activity.* methods.
- Added support for flickr.photos.* methods.
### Changed
- Conversion of boolean fields into either numeric url parameters or true/false
- Conversion of upload date fields into numeric url parameters or MySQL datetime format
- Deserialization of dates from MySQL datetime format

## 0.1.0 - 2018-11-21
### Changed
- Builder methods generation was reimplemented using a custom derive macro.
- Replaced `String` with `&str` in builders parameters and defined a needed lifetime.
- `FlickrAPI.init` is now private and called transparently when needed.

## 0.0.2 - 2018-11-16
### Changed
- Module visibility and documentation changed.

## 0.0.1 - 2018-11-15
### Added
- Added support for flickr.auth.oauth.* and flickr.activity.* methods.

[Keep a Changelog]: http://keepachangelog.com/
[Semantic Versioning]: http://semver.org/

