
use super::super::super::*;
use super::super::*;

// ---- Builder -----------------------------------------------------------------------------------

/// Builder for flickr.auth.oauth.* methods.
pub struct Builder<'a> {
    flickr: &'a mut FlickrAPI,
}

impl<'a> Builder<'a> {
    pub fn new(flickr: &mut FlickrAPI) -> Builder {
        Builder {
            flickr: flickr,
        }
    }

    pub fn check_token(&mut self) -> CheckTokenBuilder {
        CheckTokenBuilder {flickr: self.flickr}
    }
}

// ---- CheckTokenBuilder -------------------------------------------------------------------------

/// Builder for flickr.auth.oauth.checkToken 
#[derive(Builder)]
#[method = "flickr.auth.oauth.checkToken"]
#[result = "CheckTokenResult"]
pub struct CheckTokenBuilder<'a> {
    flickr: &'a mut FlickrAPI,
}

// ---- CheckTokenResult --------------------------------------------------------------------------
#[derive(Deserialize, Debug)]
pub struct CheckTokenResult {
  #[serde(default)]
  pub stat: String,

  #[serde(default)]
  pub code: i32,

  #[serde(default)]
  pub message: String,

  #[serde(default)]
  pub oauth: Option<Oauth>,
}

// ---- Oauth -------------------------------------------------------------------------------------
#[derive(Deserialize, Debug)]
pub struct Oauth {
  #[serde(deserialize_with = "string_from_content_block", default)]
  pub token: String,

  #[serde(deserialize_with = "string_from_content_block", default)]
  pub perms: String,

  pub user: User,
}

// ---- User --------------------------------------------------------------------------------------
#[derive(Deserialize, Debug)]
pub struct User {
  #[serde(default)]
  pub nsid: String,

  #[serde(default)]
  pub username: String,

  #[serde(default)]
  pub fullname: String,
}

