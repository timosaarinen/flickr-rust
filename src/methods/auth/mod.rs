
pub mod oauth;

use super::super::*;

// ---- Builder -----------------------------------------------------------------------------------

/// Builder for flickr.auth.* modules.
pub struct Builder<'a> {
    flickr: &'a mut FlickrAPI,
}

impl<'a> Builder<'a> {
    pub fn new(flickr: &mut FlickrAPI) -> Builder {
        Builder {
            flickr: flickr,
        }
    }

    pub fn oauth(&mut self) -> oauth::Builder {
        oauth::Builder::new(self.flickr)
    }
}

