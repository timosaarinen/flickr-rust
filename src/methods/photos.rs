
///
/// Methods for flickr.photos.*
///

use super::*;
use super::super::*;

// ---- Builder -----------------------------------------------------------------------------------

/// Builder for flickr.photos.* methods.
pub struct Builder<'a> {
    flickr: &'a mut FlickrAPI,
}

impl<'a> Builder<'a> {
    pub fn new(flickr: &mut FlickrAPI) -> Builder {
        Builder {
            flickr: flickr,
        }
    }

    /// Builder for method flickr.photos.addTags.
    pub fn add_tags(&mut self) -> AddTagsBuilder {
        AddTagsBuilder::new(self.flickr)
    }

    /// Builder for method flickr.photos.delete.
    pub fn delete(&mut self) -> DeleteBuilder {
        DeleteBuilder::new(self.flickr)
    }

    /// Builder for method flickr.photos.getAllContexts.
    pub fn get_all_contexts(&mut self) -> GetAllContextsBuilder {
        GetAllContextsBuilder::new(self.flickr)
    }

    /// Builder for method flickr.photos.getContactsPhotos.
    pub fn get_contacts_photos(&mut self) -> GetContactsPhotosBuilder {
        GetContactsPhotosBuilder::new(self.flickr)
    }

    /// Builder for method flickr.photos.getContactsPublicPhotos.
    pub fn get_contacts_public_photos(&mut self) -> GetContactsPublicPhotosBuilder {
        GetContactsPublicPhotosBuilder::new(self.flickr)
    }

    /// Builder for method flickr.photos.getContext.
    pub fn get_context(&mut self) -> GetContextBuilder {
        GetContextBuilder::new(self.flickr)
    }

    /// Builder for method flickr.photos.getCounts.
    pub fn get_counts(&mut self) -> GetCountsBuilder {
        GetCountsBuilder::new(self.flickr)
    }

    /// Builder for method flickr.photos.getExif.
    pub fn get_exif(&mut self) -> GetExifBuilder {
        GetExifBuilder::new(self.flickr)
    }

    /// Builder for method flickr.photos.getFavorites.
    pub fn get_favorites(&mut self) -> GetFavoritesBuilder {
        GetFavoritesBuilder::new(self.flickr)
    }

    /// Builder for method flickr.photos.getInfo.
    pub fn get_info(&mut self) -> GetInfoBuilder {
        GetInfoBuilder::new(self.flickr)
    }
    
    /// Builder for method flickr.photos.getNotInSetBuilder.
    pub fn get_not_in_set(&mut self) -> GetNotInSetBuilder {
        GetNotInSetBuilder::new(self.flickr)
    }
    
    /// Builder for method flickr.photos.getPerms.
    pub fn get_perms(&mut self) -> GetPermsBuilder {
        GetPermsBuilder::new(self.flickr)
    }
    
    /// Builder for method flickr.photos.getPopular.
    pub fn get_popular(&mut self) -> GetPopularBuilder {
        GetPopularBuilder::new(self.flickr)
    }
    
    /// Builder for method flickr.photos.getRecent.
    pub fn get_recent(&mut self) -> GetRecentBuilder {
        GetRecentBuilder::new(self.flickr)
    }
    
    /// Builder for method flickr.photos.getSizes.
    pub fn get_sizes(&mut self) -> GetSizesBuilder {
        GetSizesBuilder::new(self.flickr)
    }
    
    /// Builder for method flickr.photos.getUntagged.
    pub fn get_untagged(&mut self) -> GetUntaggedBuilder {
        GetUntaggedBuilder::new(self.flickr)
    }
    
    /// Builder for method flickr.photos.getWithGeoData.
    pub fn get_with_geo_data_builder(&mut self) -> GetWithGeoDataBuilder {
        GetWithGeoDataBuilder::new(self.flickr)
    }
    
    /// Builder for method flickr.photos.getWithoutGeoData.
    pub fn get_without_geo_data_builder(&mut self) -> GetWithoutGeoDataBuilder {
        GetWithoutGeoDataBuilder::new(self.flickr)
    }
    
    /// Builder for method flickr.photos.recentlyUpdated.
    pub fn recently_updated_builder(&mut self) -> RecentlyUpdatedBuilder {
        RecentlyUpdatedBuilder::new(self.flickr)
    }

    /// Builder for method flickr.photos.removeTag.
    pub fn remove_tag(&mut self) -> RemoveTagBuilder {
        RemoveTagBuilder::new(self.flickr)
    }
    
    /// Builder for method flickr.photos.search.
    pub fn search(&mut self) -> SearchBuilder {
        SearchBuilder::new(self.flickr)
    }
    
    /// Builder for method flickr.photos.setContentType.
    pub fn set_content_type(&mut self) -> SetContentTypeBuilder {
        SetContentTypeBuilder::new(self.flickr)
    }
    
    /// Builder for method flickr.photos.setDates.
    pub fn set_dates(&mut self) -> SetDatesBuilder {
        SetDatesBuilder::new(self.flickr)
    }
    
    /// Builder for method flickr.photos.setMeta.
    pub fn set_meta(&mut self) -> SetMetaBuilder {
        SetMetaBuilder::new(self.flickr)
    }
    
    /// Builder for method flickr.photos.setPerms.
    pub fn set_perms(&mut self) -> SetPermsBuilder {
        SetPermsBuilder::new(self.flickr)
    }
    
    /// Builder for method flickr.photos.setSafetyLevel.
    pub fn set_safety_level(&mut self) -> SetSafetyLevelBuilder {
        SetSafetyLevelBuilder::new(self.flickr)
    }
    
    /// Builder for method flickr.photos.setTags.
    pub fn set_tags(&mut self) -> SetTagsBuilder {
        SetTagsBuilder::new(self.flickr)
    }
    
}

// ---- AddTagsBuilder ----------------------------------------------------------------------------

/// Builder for flickr.photos.addTags.
#[derive(Builder)]
#[method = "flickr.photos.addTags"]
#[result = "SimpleResult"]
pub struct AddTagsBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    photo_id: &'a str,
    tags: &'a str,
}

// ---- DeleteBuilder ----------------------------------------------------------------------------

/// Builder for flickr.photos.delete.
#[derive(Builder)]
#[method = "flickr.photos.delete"]
#[result = "SimpleResult"]
pub struct DeleteBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    photo_id: &'a str,
}

// ---- GetAllContextsBuilder ---------------------------------------------------------------------

/// Builder for flickr.photos.getAllContexts.
#[derive(Builder)]
#[method = "flickr.photos.getAllContexts"]
#[result = "GetAllContextsResult"]
pub struct GetAllContextsBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    photo_id: &'a str,
}

/// Result from flickr.photos.getAllContexts.
#[derive(Deserialize, Debug)]
pub struct GetAllContextsResult {
    #[serde(default)]
    pub stat: String,

    #[serde(deserialize_with = "i32_from_value", default)]
    pub code: i32,

    #[serde(default)]
    pub message: String,
    
    #[serde(default)]
    pub set: Vec<Set>,
    
    #[serde(default)]
    pub pool: Vec<Pool>,
}

/// Aggregate of `GetAllContextsResult`.
#[derive(Deserialize, Debug)]
pub struct Set {
    #[serde(default)]
    pub id: String,

    #[serde(default)]
    pub title: String,

    // TODO: storage meta fields
}

/// Aggregate of `GetAllContextsResult`.
#[derive(Deserialize, Debug)]
pub struct Pool {
    #[serde(default)]
    pub id: String,

    #[serde(default)]
    pub title: String,

    #[serde(default)]
    pub url: String,
    
    // TODO: storage meta fields
}

// ---- GetContactsPhotosBuilder ------------------------------------------------------------------

/// Builder for flickr.photos.getContactsPhotos.
#[derive(Builder)]
#[method = "flickr.photos.getContactsPhotos"]
#[result = "GetContactsPhotosResult"]
pub struct GetContactsPhotosBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    count: Option<i32>,
    just_friends: Option<bool>,
    single_photo: Option<bool>,
    include_self: Option<bool>,
    extras: Option<String>,
}

/// Result from flickr.photos.getAllContexts.
#[derive(Deserialize, Debug)]
pub struct GetContactsPhotosResult {
    #[serde(default)]
    pub stat: String,

    #[serde(deserialize_with = "i32_from_value", default)]
    pub code: i32,

    #[serde(default)]
    pub message: String,
    
    #[serde(default)]
    pub photos: Option<Photos2>,
}

// ---- GetContactsPublicPhotosBuilder ------------------------------------------------------------

/// Builder for flickr.photos.getContactsPublicPhotos.
#[derive(Builder)]
#[method = "flickr.photos.getContactsPublicPhotos"]
#[result = "GetContactsPhotosResult"]
pub struct GetContactsPublicPhotosBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    user_id: &'a str,
    count: Option<i32>,
    just_friends: Option<bool>,
    single_photo: Option<bool>,
    include_self: Option<bool>,
    extras: Option<String>,
}

// ---- GetContextBuilder -------------------------------------------------------------------------

/// Builder for flickr.photos.GetContextBuilder.
#[derive(Builder)]
#[method = "flickr.photos.getContextBuilder"]
#[result = "GetContextResult"]
pub struct GetContextBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    photos_id: &'a str,
}

/// Result from flickr.photos.GetContextBuilder.
#[derive(Deserialize, Debug)]
pub struct GetContextResult {
    #[serde(default)]
    pub stat: String,

    #[serde(default)]
    pub code: i32,

    #[serde(default)]
    pub message: String,
    
    #[serde(default)]
    pub nextphoto: Option<PhotoNav>,
    
    #[serde(default)]
    pub prevphoto: Option<PhotoNav>,
}

// Aggregate of `GetContextResult`.
#[derive(Deserialize, Debug)]
pub struct PhotoNav {
    pub id: String,
    #[serde(default)]
    pub owner: String,
    #[serde(default)]
    pub username: String,
    #[serde(default)]
    pub title: String,
}

// ---- GetCountsBuilder -------------------------------------------------------------------------

/// Builder for flickr.photos.getCounts. Notice that this builder has special
/// methods `add_date` and `add_taken_date`. 
#[derive(Builder)]
#[method = "flickr.photos.getCounts"]
#[result = "GetCountsResult"]
pub struct GetCountsBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    dates: Option<String>,
    taken_dates: Option<String>, // TODO: Vec of MySQL datetimes instead of string list
}

impl<'a> GetCountsBuilder<'a> {
    /// Add a date to the `dates` field as a string.
    pub fn add_date(&mut self, date: DateTime<Local>) -> &'a mut GetCountsBuilder {
        let d = self.dates.clone();
        if let Some(ref dates) = d {
            let s = format!("{},{}", dates, date.timestamp());
            self.dates = Some(s);
        } else {
            let s = format!("{}", date.timestamp());
            self.dates = Some(s);
        }
        self
    }

    /// Add a date to the `taken_dates` field as a string.
    pub fn add_taken_date(&mut self, date: DateTime<Local>) -> &'a mut GetCountsBuilder {
        let d = self.taken_dates.clone();
        if let Some(ref dates) = d {
            let s = format!("{},{}", dates, date.timestamp());
            self.taken_dates = Some(s);
        } else {
            let s = format!("{}", date.timestamp());
            self.taken_dates = Some(s);
        }
        self
    }
}

/// Result from flickr.photos.GetCountsResult.
#[derive(Deserialize, Debug)]
pub struct GetCountsResult {
    #[serde(default)]
    pub stat: String,

    #[serde(deserialize_with = "i32_from_value", default)]
    pub code: i32,

    #[serde(default)]
    pub message: String,
    
    #[serde(default)]
    pub photocounts: Option<PhotoCounts>,
}

/// Aggregate of `GetCountsResult`.
#[derive(Deserialize, Debug)]
pub struct PhotoCounts {
    #[serde(default)]
    pub photocount: Vec<PhotoCount>,
}

/// Aggregate of `PhotoCounts`.
#[derive(Deserialize, Debug)]
pub struct PhotoCount {
    #[serde(deserialize_with = "i32_from_value", default)]
    pub count: i32,

    #[serde(deserialize_with = "date_from_string")]
    pub fromdate: DateTime<Local>,

    #[serde(deserialize_with = "date_from_string")]
    pub todate: DateTime<Local>,
}

// ---- GetExifBuilder ----------------------------------------------------------------------------

/// Builder for flickr.photos.getExif.
#[derive(Builder)]
#[method = "flickr.photos.getExif"]
#[result = "GetExifResult"]
pub struct GetExifBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    photo_id: &'a str,
    secret: Option<&'a str>,
}

/// Result from flickr.photos.getExif.
#[derive(Deserialize, Debug)]
pub struct GetExifResult {
    #[serde(default)]
    pub stat: String,

    #[serde(deserialize_with = "i32_from_value", default)]
    pub code: i32,

    #[serde(default)]
    pub message: String,
    
    #[serde(default)]
    pub photo: Option<ExifPhoto>,
}

/// Aggregate of `GetExifResult`.
#[derive(Deserialize, Debug)]
pub struct ExifPhoto {
    #[serde(default)]
    pub id: String,

    #[serde(default)]
    pub secret: String,

    #[serde(default)]
    pub server: String,

    #[serde(deserialize_with = "i32_from_value", default)]
    pub farm: i32,

    #[serde(default)]
    pub exif: Vec<Exif>,
}

/// Aggregate of `ExifPhoto`.
#[derive(Deserialize, Debug)]
pub struct Exif {
    #[serde(default)]
    pub tagspace: String,

    #[serde(deserialize_with = "i32_from_value", default)]
    pub tagspaceid: i32,
    
    #[serde(default)]
    pub tag: String,
    
    #[serde(default)]
    pub label: String,
    
    #[serde(deserialize_with = "string_from_content_block", default)]
    pub raw: String,

}

// ---- GetFavoritesBuilder -----------------------------------------------------------------------

/// Builder for flickr.photos.getFavorites.
#[derive(Builder)]
#[method = "flickr.photos.getFavorites"]
#[result = "GetFavoritesResult"]
pub struct GetFavoritesBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    photo_id: &'a str,
    page: Option<i32>,
    per_page: Option<i32>,
}

/// Result from flickr.photos.getFavorites.
#[derive(Deserialize, Debug)]
pub struct GetFavoritesResult {
    #[serde(default)]
    pub stat: String,

    #[serde(deserialize_with = "i32_from_value", default)]
    pub code: i32,

    #[serde(default)]
    pub message: String,
    
    #[serde(default)]
    pub photo: Option<FavoritesPhoto>,
}

/// Aggregate of `GetFavoritesResult`.
#[derive(Deserialize, Debug)]
pub struct FavoritesPhoto {
    #[serde(default)]
    pub id: String,

    #[serde(default)]
    pub secret: String,

    #[serde(default)]
    pub server: String,

    #[serde(deserialize_with = "i32_from_value", default)]
    pub farm: i32,

    #[serde(deserialize_with = "i32_from_value", default)]
    pub page: i32,

    #[serde(deserialize_with = "i32_from_value", default)]
    pub pages: i32,

    #[serde(deserialize_with = "i32_from_value", default)]
    pub perpage: i32,

    #[serde(deserialize_with = "i32_from_value", default)]
    pub total: i32,
    
    #[serde(default)]
    pub person: Vec<FavoritesPhotoPerson>
}

/// Aggregate of `FavoritesPhoto`.
#[derive(Deserialize, Debug)]
pub struct FavoritesPhotoPerson {
    #[serde(default)]
    pub nsid: String,

    #[serde(default)]
    pub username: String,

    #[serde(default)]
    pub realname: String,

    #[serde(deserialize_with = "date_from_string")]
    pub favedate: DateTime<Local>,

    #[serde(default)]
    pub iconserver: String,

    #[serde(deserialize_with = "i32_from_value", default)]
    pub iconfarm: i32,

    #[serde(deserialize_with = "bool_from_int", default)]
    pub contact: bool,

    #[serde(deserialize_with = "bool_from_int", default)]
    pub friend: bool,

    #[serde(deserialize_with = "bool_from_int", default)]
    pub family: bool,

}

// ---- GetInfoBuilder ----------------------------------------------------------------------------

/// Builder for flickr.photos.getInfo.
#[derive(Builder)]
#[method = "flickr.photos.getInfo"]
#[result = "GetInfoResult"]
pub struct GetInfoBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    photo_id: &'a str,
    secret: Option<&'a str>,
}

/// Result from flickr.photos.getFavorites.
#[derive(Deserialize, Debug)]
pub struct GetInfoResult {
    #[serde(default)]
    pub stat: String,

    #[serde(deserialize_with = "i32_from_value", default)]
    pub code: i32,

    #[serde(default)]
    pub message: String,
    
    #[serde(default)]
    pub photo: Option<PhotoInfo>,
}

#[derive(Deserialize, Debug)]
pub struct PhotoInfo {
    pub id: String,
    #[serde(default)]
    pub secret: String,
    #[serde(default)]
    pub server: String,
    #[serde(deserialize_with = "i32_from_value", default)]
    pub farm: i32,
    #[serde(deserialize_with = "date_from_string")]
    pub dateuploaded: DateTime<Local>,
    #[serde(deserialize_with = "bool_from_int", default)]
    pub isfavorite: bool,
    #[serde(default)]
    pub license: String,
    #[serde(default)]
    pub safety_level: String,
    #[serde(deserialize_with = "i32_from_value", default)]
    pub rotation: i32,
    #[serde(default)]
    pub originalsecret: String,
    #[serde(default)]
    pub originalformat: String,
    pub owner: PhotoInfoOwner,
    #[serde(deserialize_with = "string_from_content_block", default)]
    pub title: String,
    #[serde(deserialize_with = "string_from_content_block", default)]
    pub description: String,
    pub visibility: PhotoInfoVisibility,    
    pub dates: PhotoInfoDates,
    #[serde(deserialize_with = "i32_from_value", default)]
    pub views: i32,
    pub editability: PhotoInfoEditability,
    pub publiceditability: PhotoInfoEditability,
    pub usage: PhotoInfoUsage,
    #[serde(deserialize_with = "i32_from_content_block", default)]
    pub comments: i32,
    pub notes: PhotoInfoNotes,
    pub people: PhotoInfoPeople,
    pub tags: PhotoInfoTags,
    pub urls: PhotoInfoUrls,
    pub media: String,
}

#[derive(Deserialize, Debug)]
pub struct PhotoInfoOwner {
    #[serde(default)]
    pub nsid: String,
    #[serde(default)]
    pub username: String,
    #[serde(default)]
    pub realname: String,
    #[serde(default)]
    pub location: String,
    #[serde(default)]
    pub iconserver: String,
    #[serde(deserialize_with = "i32_from_value", default)]
    pub iconfarm: i32,
    #[serde(default)]
    pub path_alias: String,
}

#[derive(Deserialize, Debug)]
pub struct PhotoInfoVisibility {
    #[serde(deserialize_with = "bool_from_int", default)]
    pub ispublic: bool,
    #[serde(deserialize_with = "bool_from_int", default)]
    pub isfriend: bool,
    #[serde(deserialize_with = "bool_from_int", default)]
    pub isfamily: bool,
}

#[derive(Deserialize, Debug)]
pub struct PhotoInfoDates {
    #[serde(deserialize_with = "date_from_string")]
    pub posted: DateTime<Local>,
    #[serde(deserialize_with = "date_from_mysql_string")]
    pub taken: DateTime<Local>,
    #[serde(deserialize_with = "i32_from_value")]
    pub takengranularity: i32,
    #[serde(deserialize_with = "date_from_string")]
    pub takenunknown: DateTime<Local>,
    #[serde(deserialize_with = "date_from_string")]
    pub lastupdate: DateTime<Local>,
}

#[derive(Deserialize, Debug)]
pub struct PhotoInfoEditability {
    #[serde(deserialize_with = "bool_from_int", default)]
    pub cancomment: bool,
    #[serde(deserialize_with = "bool_from_int", default)]
    pub canaddmeta: bool,
}

#[derive(Deserialize, Debug)]
pub struct PhotoInfoUsage {
    #[serde(deserialize_with = "bool_from_int", default)]
    pub candownload: bool,
    #[serde(deserialize_with = "bool_from_int", default)]
    pub canblog: bool,
    #[serde(deserialize_with = "bool_from_int", default)]
    pub canprint: bool,
    #[serde(deserialize_with = "bool_from_int", default)]
    pub canshare: bool,
}

#[derive(Deserialize, Debug)]
pub struct PhotoInfoNotes {
    pub note: Vec<PhotoInfoNote>,
}

#[derive(Deserialize, Debug)]
pub struct PhotoInfoNote {
    // TODO
}

#[derive(Deserialize, Debug)]
pub struct PhotoInfoPeople {
    #[serde(deserialize_with = "bool_from_int", default)]
    pub haspeople: bool,
}

#[derive(Deserialize, Debug)]
pub struct PhotoInfoTags {
    pub tag: Vec<PhotoInfoTag>,
}

#[derive(Deserialize, Debug)]
pub struct PhotoInfoTag {
    #[serde(default)]
    pub id: String,
    #[serde(default)]
    pub author: String,
    #[serde(default)]
    pub authorname: String,
    #[serde(default)]
    pub raw: String,
    #[serde(default,rename="_content")]
    pub content: String,
    #[serde(deserialize_with = "i32_from_value", default)]
    pub machine_tag: i32,
}

#[derive(Deserialize, Debug)]
pub struct PhotoInfoUrls {
    pub url: Vec<PhotoInfoUrl>,
}

#[derive(Deserialize, Debug)]
pub struct PhotoInfoUrl {
    #[serde(default)]
    pub r#type: String,
    #[serde(default,rename="_content")]
    pub content: String,
}

// ---- GetNotInSetBuilder ------------------------------------------------------------------------

/// Builder for flickr.photos.getNotInSet.
#[derive(Builder)]
#[method = "flickr.photos.getNotInSet"]
#[result = "GetNotInSetResult"]
pub struct GetNotInSetBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    max_upload_date: Option<DateTime<Local>>,
    min_upload_date: Option<DateTime<Local>>,
    min_taken_date: Option<DateTime<Local>>,
    max_taken_date: Option<DateTime<Local>>,
    privacy_filter: Option<i8>,
    media: Option<&'a str>,
    extras: Option<&'a str>,
    per_page: Option<i32>,
    page: Option<i32>,
}

/// Result from flickr.photos.getNotInSet.
#[derive(Deserialize, Debug)]
pub struct GetNotInSetResult {
    #[serde(default)]
    pub stat: String,

    #[serde(deserialize_with = "i32_from_value", default)]
    pub code: i32,

    #[serde(default)]
    pub message: String,
    
    #[serde(default)]
    pub photos: Option<Photos>,
}


// ---- GetPermsBuilder ---------------------------------------------------------------------------

/// Builder for flickr.photos.getPerms.
#[derive(Builder)]
#[method = "flickr.photos.getPerms"]
#[result = "GetPermsResult"]
pub struct GetPermsBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    photo_id: &'a str,
}

/// Result from flickr.photos.getPerms.
#[derive(Deserialize, Debug)]
pub struct GetPermsResult {
    #[serde(default)]
    pub stat: String,

    #[serde(deserialize_with = "i32_from_value", default)]
    pub code: i32,

    #[serde(default)]
    pub message: String,
    
    #[serde(default)]
    pub perms: Option<Perms>,
}

/// Result from `GetPermsResult`.
#[derive(Deserialize, Debug)]
pub struct Perms {
    #[serde(default)]
    pub id: String,

    #[serde(deserialize_with = "bool_from_int", default)]
    pub ispublic: bool,
    
    #[serde(deserialize_with = "bool_from_int", default)]
    pub isfriend: bool,
    
    #[serde(deserialize_with = "bool_from_int", default)]
    pub isfamily: bool,
    
    #[serde(deserialize_with = "i32_from_value", default)]
    pub permcomment: i32,
    
    #[serde(deserialize_with = "i32_from_value", default)]
    pub permaddmeta: i32,
}

// ---- GetPopularBuilder -------------------------------------------------------------------------

/// Builder for flickr.photos.getPopular.
#[derive(Builder)]
#[method = "flickr.photos.getPopular"]
#[result = "GetPopularResult"]
pub struct GetPopularBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    user_id: Option<&'a str>,
    sort: Option<bool>,
    extras: Option<&'a str>,
    per_page: Option<i32>,
    page: Option<i32>,
}

/// Result from flickr.photos.getPopular.
#[derive(Deserialize, Debug)]
pub struct GetPopularResult {
    #[serde(default)]
    pub stat: String,

    #[serde(deserialize_with = "i32_from_value", default)]
    pub code: i32,

    #[serde(default)]
    pub message: String,
    
    #[serde(default)]
    pub photos: Option<Photos>,
}

// ---- GetRecentBuilder --------------------------------------------------------------------------

/// Builder for flickr.photos.getRecent.
#[derive(Builder)]
#[method = "flickr.photos.getRecent"]
#[result = "GetRecentResult"]
pub struct GetRecentBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    extras: Option<&'a str>,
    per_page: Option<i32>,
    page: Option<i32>,
}

/// Result from flickr.photos.getRecent.
#[derive(Deserialize, Debug)]
pub struct GetRecentResult {
    #[serde(default)]
    pub stat: String,

    #[serde(deserialize_with = "i32_from_value", default)]
    pub code: i32,

    #[serde(default)]
    pub message: String,
    
    #[serde(default)]
    pub photos: Option<Photos>,
}

// ---- GetSizesBuilder ---------------------------------------------------------------------------

/// Builder for flickr.photos.getSizes.
#[derive(Builder)]
#[method = "flickr.photos.getSizes"]
#[result = "GetSizesResult"]
pub struct GetSizesBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    photo_id: Option<&'a str>,
}

/// Result from flickr.photos.getSizes.
#[derive(Deserialize, Debug)]
pub struct GetSizesResult {
    #[serde(default)]
    pub stat: String,

    #[serde(deserialize_with = "i32_from_value", default)]
    pub code: i32,

    #[serde(default)]
    pub message: String,
    
    #[serde(default)]
    pub sizes: Option<Sizes>,
}

/// Aggregate of `GetSizesResult`.
#[derive(Deserialize, Debug)]
pub struct Sizes {
    #[serde(deserialize_with = "bool_from_int", default)]
    pub canblog: bool,

    #[serde(deserialize_with = "bool_from_int", default)]
    pub canprint: bool,

    #[serde(deserialize_with = "bool_from_int", default)]
    pub candownload: bool,

    pub size: Vec<Size>,
}

/// Aggregate of `Sizes`.
#[derive(Deserialize, Debug)]
pub struct Size {
    #[serde(default)]
    pub label: String,

    #[serde(deserialize_with = "i32_from_value", default)]
    pub width: i32,

    #[serde(deserialize_with = "i32_from_value", default)]
    pub height: i32,

    #[serde(default)]
    pub source: String,

    #[serde(default)]
    pub url: String,

    #[serde(default)]
    pub media: String,
}

// ---- GetUntaggedBuilder ------------------------------------------------------------------------

/// Builder for flickr.photos.getUntagged.
#[derive(Builder)]
#[method = "flickr.photos.getUntagged"]
#[result = "GetUntaggedResult"]
pub struct GetUntaggedBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    max_upload_date: Option<DateTime<Local>>,
    min_upload_date: Option<DateTime<Local>>,
    min_taken_date: Option<DateTime<Local>>,
    max_taken_date: Option<DateTime<Local>>,
    privacy_filter: Option<i8>,
    media: Option<&'a str>,
    extras: Option<&'a str>,
    per_page: Option<i32>,
    page: Option<i32>,
}

/// Result from flickr.photos.getUntagged.
#[derive(Deserialize, Debug)]
pub struct GetUntaggedResult {
    #[serde(default)]
    pub stat: String,

    #[serde(deserialize_with = "i32_from_value", default)]
    pub code: i32,

    #[serde(default)]
    pub message: String,
    
    #[serde(default)]
    pub photos: Option<Photos>,
}

// ---- GetWithGeoDataBuilder ---------------------------------------------------------------------

/// Builder for flickr.photos.getWithGeoData.
#[derive(Builder)]
#[method = "flickr.photos.getWithGeoData"]
#[result = "GetWithGeoDataResult"]
pub struct GetWithGeoDataBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    max_upload_date: Option<DateTime<Local>>,
    min_upload_date: Option<DateTime<Local>>,
    min_taken_date: Option<DateTime<Local>>,
    max_taken_date: Option<DateTime<Local>>,
    privacy_filter: Option<i8>,
    media: Option<&'a str>,
    extras: Option<&'a str>,
    per_page: Option<i32>,
    page: Option<i32>,
}

/// Result from flickr.photos.getWithGeoData.
#[derive(Deserialize, Debug)]
pub struct GetWithGeoDataResult {
    #[serde(default)]
    pub stat: String,

    #[serde(deserialize_with = "i32_from_value", default)]
    pub code: i32,

    #[serde(default)]
    pub message: String,
    
    #[serde(default)]
    pub photos: Option<Photos>,
}

// ---- GetWithoutGeoDataBuilder ------------------------------------------------------------------

/// Builder for flickr.photos.getWithoutGeoData.
#[derive(Builder)]
#[method = "flickr.photos.getWithoutGeoData"]
#[result = "GetWithoutGeoDataResult"]
pub struct GetWithoutGeoDataBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    max_upload_date: Option<DateTime<Local>>,
    min_upload_date: Option<DateTime<Local>>,
    min_taken_date: Option<DateTime<Local>>,
    max_taken_date: Option<DateTime<Local>>,
    privacy_filter: Option<i8>,
    media: Option<&'a str>,
    extras: Option<&'a str>,
    per_page: Option<i32>,
    page: Option<i32>,
}

/// Result from flickr.photos.getWithoutGeoData.
#[derive(Deserialize, Debug)]
pub struct GetWithoutGeoDataResult {
    #[serde(default)]
    pub stat: String,

    #[serde(deserialize_with = "i32_from_value", default)]
    pub code: i32,

    #[serde(default)]
    pub message: String,
    
    #[serde(default)]
    pub photos: Option<Photos>,
}

// ---- RecentlyUpdatedBuilder --------------------------------------------------------------------

/// Builder for flickr.photos.recentlyUpdated.
#[derive(Builder)]
#[method = "flickr.photos.recentlyUpdated"]
#[result = "RecentlyUpdatedResult"]
pub struct RecentlyUpdatedBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    max_upload_date: Option<DateTime<Local>>,
    min_upload_date: Option<DateTime<Local>>,
    min_taken_date: Option<DateTime<Local>>,
    max_taken_date: Option<DateTime<Local>>,
    privacy_filter: Option<i8>,
    media: Option<&'a str>,
    extras: Option<&'a str>,
    per_page: Option<i32>,
    page: Option<i32>,
}

/// Result from flickr.photos.recentlyUpdated.
#[derive(Deserialize, Debug)]
pub struct RecentlyUpdatedResult {
    #[serde(default)]
    pub stat: String,

    #[serde(deserialize_with = "i32_from_value", default)]
    pub code: i32,

    #[serde(default)]
    pub message: String,
    
    #[serde(default)]
    pub photos: Option<Photos>,
}


// ---- RemoveTagBuilder --------------------------------------------------------------------------

/// Builder for flickr.photos.removeTag.
#[derive(Builder)]
#[method = "flickr.photos.removeTag"]
#[result = "SimpleResult"]
pub struct RemoveTagBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    photo_id: &'a str,
    tag_id: &'a str,
}

// ---- SearchBuilder -----------------------------------------------------------------------------

/// Builder for flickr.photos.search.
#[derive(Builder)]
#[method = "flickr.photos.search"]
#[result = "SearchResult"]
pub struct SearchBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    user_id: Option<&'a str>,
    tags: Option<&'a str>,
    tag_mode: Option<&'a str>,
    text: Option<&'a str>,
    min_upload_date: Option<DateTime<Local>>,
    max_upload_date: Option<DateTime<Local>>,
    min_taken_date: Option<DateTime<Local>>,
    max_taken_date: Option<DateTime<Local>>,
    license: Option<&'a str>,
    sort: Option<&'a str>,
    privacy_filter: Option<&'a str>,
    bbox: Option<&'a str>,
    accuracy: Option<i8>,
    safe_search: Option<i8>,
    content_type: Option<i8>,
    machine_tags: Option<&'a str>,
    machine_tags_mode: Option<&'a str>,
    group_id: Option<&'a str>,
    contacts: Option<&'a str>,
    woe_id: Option<&'a str>,
    place_id: Option<&'a str>,
    media: Option<&'a str>,
    had_geo: Option<bool>,
    geo_context: Option<i8>,
    lat: Option<f64>,
    lon: Option<f64>,
    radius: Option<i32>,
    radius_units: Option<&'a str>,
    #[truebool]
    is_commons: Option<bool>, // TODO: true/false?
    #[truebool]
    is_gallery: Option<bool>, // TODO: true/false?
    #[truebool]
    is_getty: Option<bool>, // TODO: true/false?
    extras: Option<&'a str>,
    per_page: Option<i32>,
    page: Option<i32>,
}

/// Result from flickr.photos.search.
#[derive(Deserialize, Debug)]
pub struct SearchResult {
    #[serde(default)]
    pub stat: String,

    #[serde(deserialize_with = "i32_from_value", default)]
    pub code: i32,

    #[serde(default)]
    pub message: String,
    
    #[serde(default)]
    pub photos: Option<Photos>,
}

// ---- SetContentTypeBuilder ---------------------------------------------------------------------

/// Builder for flickr.photos.setContentType.
#[derive(Builder)]
#[method = "flickr.photos.setContentType"]
#[result = "SimpleResult"]
pub struct SetContentTypeBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    photo_id: &'a str,
    content_type: &'a str,
}

// TODO: actually results more than SimpleResult

// ---- SetDatesBuilder ---------------------------------------------------------------------------

/// Builder for flickr.photos.setDates.
#[derive(Builder)]
#[method = "flickr.photos.setDates"]
#[result = "SimpleResult"]
pub struct SetDatesBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    photo_id: &'a str,
    date_posted: Option<DateTime<Local>>,
    date_taken: Option<DateTime<Local>>,
    date_taken_granularity: Option<i32>,
}

// ---- SetMetaBuilder ----------------------------------------------------------------------------

/// Builder for flickr.photos.setMeta.
#[derive(Builder)]
#[method = "flickr.photos.setMeta"]
#[result = "SimpleResult"]
pub struct SetMetaBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    photo_id: &'a str,
    title: Option<&'a str>,
    description: Option<&'a str>,
}

// ---- SetPermsBuilder ---------------------------------------------------------------------------

/// Builder for flickr.photos.setPerms.
#[derive(Builder)]
#[method = "flickr.photos.setPerms"]
#[result = "SimpleResult"]
pub struct SetPermsBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    photo_id: &'a str,
    is_public: bool,
    is_friend: bool,
    is_family: bool,
    perm_comment: Option<i32>,
    perm_addmeta: Option<i32>,
}

// TODO: actually results more than SimpleResult

// ---- SetSafetyLevelBuilder ---------------------------------------------------------------------

/// Builder for flickr.photos.setSafetyLevel.
#[derive(Builder)]
#[method = "flickr.photos.setSafetyLevel"]
#[result = "SimpleResult"]
pub struct SetSafetyLevelBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    photo_id: &'a str,
    safety_level: Option<i32>,
    hidden: Option<bool>,
}

// TODO: actually results more than SimpleResult

// ---- SetTagsBuilder ----------------------------------------------------------------------------

/// Builder for flickr.photos.setTags.
#[derive(Builder)]
#[method = "flickr.photos.setTags"]
#[result = "SimpleResult"]
pub struct SetTagsBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    photo_id: &'a str,
    tags: Option<&'a str>,
}

// ---- tests -------------------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use super::*;

    fn setup(expected_response: &str) -> FlickrAPI {
        let mut flickr = FlickrAPI::new("", "");
        flickr.test_response = Some(expected_response.into());
        return flickr;
    }

    #[test]
    fn add_tags() {
        let mut flickr = setup(r#"
            {"stat":"ok"}
        "#);
 
        let r = flickr.photos()
                      .add_tags()
                      .photo_id("123456789")
                      .tags("aa,bb")
                      .perform()
                      .unwrap();
        assert_eq!(r.stat, "ok");
    }

    #[test]
    fn delete() {
        let mut flickr = setup(r#"
            {"stat":"ok"}
        "#);
 
        let r = flickr.photos()
                      .delete()
                      .photo_id("123456789")
                      .perform()
                      .unwrap();
        assert_eq!(r.stat, "ok");
    }

    #[test]
    fn get_all_contexts() {
        let mut flickr = setup(r#"
          {
              "set": [
                  {
                      "title": "set_1_title",
                      "id": "set_1_id",
                      "primary": "set_1_primary",
                      "secret": "set_1_secret",
                      "server": "set_1_server",
                      "farm": 1,
                      "view_count": "2",
                      "comment_count": "3",
                      "count_photo": 4,
                      "count_video": 5
                  }
              ],
              "pool": [
                  {
                      "title": "pool_1_title",
                      "url": "pool_1_url",
                      "id": "pool_1_id",
                      "iconserver": "pool_1_iconserver",
                      "iconfarm": 1,
                      "members": "2",
                      "pool_count": "3"
                  },
                  {
                      "title": "pool_2_title",
                      "url": "pool_2_url",
                      "id": "pool_2_id",
                      "iconserver": "pool_2_iconserver",
                      "iconfarm": 2,
                      "members": "3",
                      "pool_count": "4"
                  }
              ],
              "stat": "ok"
          }
        "#);
 
        let r = flickr.photos()
                      .get_all_contexts()
                      .photo_id("123456789")
                      .perform()
                      .unwrap();
        assert_eq!(r.stat, "ok");
        assert_eq!(r.set.get(0).unwrap().title, "set_1_title");
        assert_eq!(r.set.get(0).unwrap().id, "set_1_id");
        assert_eq!(r.pool.get(0).unwrap().title, "pool_1_title");
        assert_eq!(r.pool.get(0).unwrap().id, "pool_1_id");
        assert_eq!(r.pool.get(0).unwrap().url, "pool_1_url");
        assert_eq!(r.pool.get(1).unwrap().title, "pool_2_title");
        assert_eq!(r.pool.get(1).unwrap().id, "pool_2_id");
        assert_eq!(r.pool.get(1).unwrap().url, "pool_2_url");
    }

    #[test]
    fn get_contacts_photos() {
        let mut flickr = setup(r#"
            {
                "photos": {
                    "photo": [
                        {
                            "id": "photo_1_id",
                            "secret": "photo_1_secret",
                            "server": "photo_1_server",
                            "farm": 1,
                            "owner": "photo_1_owner",
                            "username": "photo_1_username",
                            "title": "photo_1_title",
                            "ispublic": 1,
                            "isfriend": 1,
                            "isfamily": 1
                        }
                    ],
                    "total": 1,
                    "page": 1,
                    "per_page": 1,
                    "pages": 1
                },
                "stat": "ok"
            }
        "#);
 
        let r = flickr.photos()
                      .get_contacts_photos()
                      .perform()
                      .unwrap();
        assert_eq!(r.stat, "ok");
        if let Some(ref photos) = r.photos {
            assert_eq!(photos.photo.len(), photos.total as usize);
            assert_eq!(photos.photo.get(0).unwrap().id, "photo_1_id");
            assert_eq!(photos.photo.get(0).unwrap().secret, "photo_1_secret");
            assert_eq!(photos.photo.get(0).unwrap().server, "photo_1_server");
            assert_eq!(photos.photo.get(0).unwrap().farm, 1);
            assert_eq!(photos.photo.get(0).unwrap().owner, "photo_1_owner");
            assert_eq!(photos.photo.get(0).unwrap().username, "photo_1_username");
            assert_eq!(photos.photo.get(0).unwrap().title, "photo_1_title");
            assert_eq!(photos.photo.get(0).unwrap().ispublic, true);
            assert_eq!(photos.photo.get(0).unwrap().isfriend, true);
            assert_eq!(photos.photo.get(0).unwrap().isfamily, true);
        } else {
            assert!(false);
        }
    }

    #[test]
    fn get_contacts_public_photos() {
        let mut flickr = setup(r#"
            {
                "photos": {
                    "photo": [
                        {
                            "id": "photo_1_id",
                            "secret": "photo_1_secret",
                            "server": "photo_1_server",
                            "farm": 5,
                            "owner": "photo_1_owner",
                            "username": "photo_1_username",
                            "title": "photo_1_title",
                            "ispublic": 1,
                            "isfriend": 1,
                            "isfamily": 1
                        },
                        {
                            "id": "photo_2_id",
                            "secret": "photo_1_secret",
                            "server": "photo_1_server",
                            "farm": 2,
                            "owner": "photo_1_owner",
                            "username": "photo_1_username",
                            "title": "photo_1_title",
                            "ispublic": 0,
                            "isfriend": 0,
                            "isfamily": 0
                        }
                    ],
                    "total": 2,
                    "page": 1,
                    "per_page": 10,
                    "pages": 1
                },
                "stat": "ok"
            }
        "#);
 
        let r = flickr.photos()
                      .get_contacts_photos()
                      .perform()
                      .unwrap();
        assert_eq!(r.stat, "ok");
        if let Some(ref photos) = r.photos {
            assert_eq!(photos.photo.len(), photos.total as usize);
            assert_eq!(photos.photo.get(0).unwrap().id, "photo_1_id");
            assert_eq!(photos.photo.get(0).unwrap().secret, "photo_1_secret");
            assert_eq!(photos.photo.get(0).unwrap().server, "photo_1_server");
            assert_eq!(photos.photo.get(0).unwrap().farm, 5);
            assert_eq!(photos.photo.get(0).unwrap().owner, "photo_1_owner");
            assert_eq!(photos.photo.get(0).unwrap().username, "photo_1_username");
            assert_eq!(photos.photo.get(0).unwrap().title, "photo_1_title");
            assert_eq!(photos.photo.get(0).unwrap().ispublic, true);
            assert_eq!(photos.photo.get(0).unwrap().isfriend, true);
            assert_eq!(photos.photo.get(0).unwrap().isfamily, true);
            assert_eq!(photos.photo.get(1).unwrap().id, "photo_2_id");
        } else {
            assert!(false);
        }
    }

    #[test]
    fn get_counts() {
        let mut flickr = setup(r#"
            {
                "photocounts": {
                    "photocount": [
                        {
                            "count": "4",
                            "fromdate": "1527811200",
                            "todate": "1533081600"
                        }
                    ]
                },
                "stat": "ok"
            }
        "#);
 
        let r = flickr
                    .photos()
                    .get_counts()
                    .add_date(Utc.ymd(2018, 6, 1).and_hms(0, 0, 0).with_timezone(&Local))
                    .add_date(Utc.ymd(2018, 8, 1).and_hms(0, 0, 0).with_timezone(&Local))
                    .perform()
                    .unwrap();
        assert_eq!(r.stat, "ok");
        if let Some(ref photocounts) = r.photocounts {
            assert_eq!(photocounts.photocount.len(), 1);
            assert_eq!(photocounts.photocount.get(0).unwrap().count, 4);
        } else {
            assert!(false);
        }
    }

    #[test]
    fn get_exif() {
        let mut flickr = setup(r#"
            {
                "photo": {
                    "id": "photo_id",
                    "secret": "photo_secret",
                    "server": "photo_server",
                    "farm": 5,
                    "camera": "photo_camera",
                    "exif": [
                        {
                            "tagspace": "exif_1_space",
                            "tagspaceid": 1,
                            "tag": "exif_1_tag",
                            "label": "exif_1_label",
                            "raw": {
                                "_content": "exif_1_raw"
                            }
                        },
                        {
                            "tagspace": "exif_2_space",
                            "tagspaceid": 2,
                            "tag": "exif_2_tag",
                            "label": "exif_2_label",
                            "raw": {
                                "_content": "exif_2_raw"
                            }
                        }
                    ]
                },
                "stat": "ok"
            }
        "#);
 
        let r = flickr
                    .photos()
                    .get_exif()
                    .photo_id("123456")
                    .perform()
                    .unwrap();
        assert_eq!(r.stat, "ok");
        if let Some(ref photo) = r.photo {
            assert_eq!(photo.id, "photo_id");
            assert_eq!(photo.secret, "photo_secret");
            assert_eq!(photo.exif.len(), 2);
            assert_eq!(photo.exif.get(0).unwrap().tagspace, "exif_1_space");
            assert_eq!(photo.exif.get(0).unwrap().tagspaceid, 1);
            assert_eq!(photo.exif.get(0).unwrap().label, "exif_1_label");
            assert_eq!(photo.exif.get(0).unwrap().raw, "exif_1_raw");
        } else {
            assert!(false);
        }
    }

    #[test]
    fn get_favorites() {
        let mut flickr = setup(r#"
              {
                  "photo": {
                      "person": [
                          {
                              "nsid": "person_1_nsid",
                              "username": "person_1_username",
                              "realname": "person_1_realname",
                              "favedate": "999999999",
                              "iconserver": "1",
                              "iconfarm": 2,
                              "contact": 1,
                              "friend": 1,
                              "family": 1
                          },
                          {
                              "nsid": "person_2_nsid",
                              "username": "person_2_username",
                              "realname": "person_2_realname",
                              "favedate": "999999999",
                              "iconserver": "3",
                              "iconfarm": 4,
                              "contact": 0,
                              "friend": 0,
                              "family": 0
                          }
                      ],
                      "id": "123456",
                      "secret": "666",
                      "server": "5",
                      "farm": 6,
                      "page": 1,
                      "pages": 1,
                      "perpage": 10,
                      "total": "2"
                  },
                  "stat": "ok"
              }
        "#);
 
        let r = flickr
                    .photos()
                    .get_favorites()
                    .photo_id("123456")
                    .perform()
                    .unwrap();
        assert_eq!(r.stat, "ok");
    }

    #[test]
    fn get_info() {
        let mut flickr = setup(r#"
            {
              "photo": {
                "id": "photo_id",
                "secret": "photo_secret",
                "server": "photo_server",
                "farm": 5,
                "dateuploaded": "1543257119",
                "isfavorite": 1,
                "license": "0",
                "safety_level": "1",
                "rotation": 90,
                "originalsecret": "photo_originalsecret",
                "originalformat": "jpg",
                "owner": {
                  "nsid": "owner_nsid",
                  "username": "owner_username",
                  "realname": "owner_realname",
                  "location": "owner_location",
                  "iconserver": "owner_iconserver",
                  "iconfarm": 8,
                  "path_alias": "owner_path_alias"
                },
                "title": {
                  "_content": "photo_title"
                },
                "description": {
                  "_content": "photo_description"
                },
                "visibility": {
                  "ispublic": 1,
                  "isfriend": 1,
                  "isfamily": 1
                },
                "dates": {
                  "posted": "1543257120",
                  "taken": "2018-11-26 18:32:01",
                  "takengranularity": "8",
                  "takenunknown": "1543257123",
                  "lastupdate": "1543257124"
                },
                "views": "455",
                "editability": {
                  "cancomment": 1,
                  "canaddmeta": 1
                },
                "publiceditability": {
                  "cancomment": 1,
                  "canaddmeta": 1
                },
                "usage": {
                  "candownload": 1,
                  "canblog": 1,
                  "canprint": 1,
                  "canshare": 1
                },
                "comments": {
                  "_content": "2"
                },
                "notes": {
                  "note": [
                    
                  ]
                },
                "people": {
                  "haspeople": 1
                },
                "tags": {
                  "tag": [
                    {
                      "id": "tag_id",
                      "author": "tag_author",
                      "authorname": "tag_authorname",
                      "raw": "tag_raw",
                      "_content": "tag_content",
                      "machine_tag": 999
                    }
                  ]
                },
                "urls": {
                  "url": [
                    {
                      "type": "photopage",
                      "_content": "http://abc.cde"
                    }
                  ]
                },
                "media": "photo"
              },
              "stat": "ok"
            }
        "#);
 
        let r = flickr
                    .photos()
                    .get_info()
                    .photo_id("12345678")
                    .perform()
                    .unwrap();
        assert_eq!(r.stat, "ok");
        if let Some(p) = r.photo {
            assert_eq!(p.id, "photo_id");
            assert_eq!(p.secret, "photo_secret");
            assert_eq!(p.server, "photo_server");
            assert_eq!(p.farm, 5);
            assert_eq!(p.dateuploaded, Local.ymd(2018, 11, 26).and_hms(20, 31, 59));
            assert_eq!(p.isfavorite, true);
            assert_eq!(p.license, "0");
            assert_eq!(p.safety_level, "1");
            assert_eq!(p.rotation, 90);
            assert_eq!(p.originalsecret, "photo_originalsecret");
            assert_eq!(p.originalformat, "jpg");
            assert_eq!(p.owner.nsid, "owner_nsid");
            assert_eq!(p.owner.username, "owner_username");
            assert_eq!(p.owner.realname, "owner_realname");
            assert_eq!(p.owner.location, "owner_location");
            assert_eq!(p.owner.iconfarm, 8);
            assert_eq!(p.owner.path_alias, "owner_path_alias");
            assert_eq!(p.title, "photo_title");
            assert_eq!(p.description, "photo_description");
            assert_eq!(p.visibility.ispublic, true);
            assert_eq!(p.visibility.isfriend, true);
            assert_eq!(p.visibility.isfamily, true);
            assert_eq!(p.dates.posted,           Local.ymd(2018, 11, 26).and_hms(20, 32, 0));
            assert_eq!(p.dates.taken,            Local.ymd(2018, 11, 26).and_hms(20, 32, 1));
            assert_eq!(p.dates.takengranularity, 8);
            assert_eq!(p.dates.takenunknown,     Local.ymd(2018, 11, 26).and_hms(20, 32, 3));
            assert_eq!(p.dates.lastupdate,       Local.ymd(2018, 11, 26).and_hms(20, 32, 4));
            assert_eq!(p.views, 455);
            assert_eq!(p.editability.cancomment, true);
            assert_eq!(p.editability.canaddmeta, true);
            assert_eq!(p.publiceditability.cancomment, true);
            assert_eq!(p.publiceditability.canaddmeta, true);
            assert_eq!(p.usage.candownload, true);
            assert_eq!(p.usage.canblog, true);
            assert_eq!(p.usage.canprint, true);
            assert_eq!(p.usage.canprint, true);
            assert_eq!(p.comments, 2);
            assert_eq!(p.people.haspeople, true);
            // TODO: notes
            assert_eq!(p.tags.tag.get(0).unwrap().id, "tag_id");
            assert_eq!(p.tags.tag.get(0).unwrap().author, "tag_author");
            assert_eq!(p.tags.tag.get(0).unwrap().authorname, "tag_authorname");
            assert_eq!(p.tags.tag.get(0).unwrap().raw, "tag_raw");
            assert_eq!(p.tags.tag.get(0).unwrap().content, "tag_content");
            assert_eq!(p.tags.tag.get(0).unwrap().machine_tag, 999);
            assert_eq!(p.urls.url.get(0).unwrap().r#type, "photopage");
            assert_eq!(p.urls.url.get(0).unwrap().content, "http://abc.cde");
            assert_eq!(p.media, "photo");
        }
    }

    #[test]
    fn get_not_in_set() {
        let mut flickr = setup(r#"
            {
              "photos": {
                "page": 1,
                "pages": 1,
                "perpage": 100,
                "total": "35",
                "photo": [
                  {
                    "id": "photo_1_id",
                    "owner": "photo_1_owner",
                    "secret": "photo_1_secret",
                    "server": "photo_1_server",
                    "farm": 4,
                    "title": "photo_1_title",
                    "ispublic": 1,
                    "isfriend": 1,
                    "isfamily": 1
                  },
                  {
                    "id": "photo_2_id",
                    "owner": "photo_2_owner",
                    "secret": "photo_2_secret",
                    "server": "photo_2_server",
                    "farm": 4,
                    "title": "photo_2_title",
                    "ispublic": 0,
                    "isfriend": 0,
                    "isfamily": 0
                  }
                ]
              },
              "stat": "ok"
            }
        "#);
 
        let r = flickr
                    .photos()
                    .get_not_in_set()
                    .perform()
                    .unwrap();
        assert_eq!(r.stat, "ok");
        if let Some(photos) = r.photos {
            assert_eq!(photos.page, 1);
            assert_eq!(photos.pages, 1);
            assert_eq!(photos.perpage, 100);
            assert_eq!(photos.total, 35);
            assert_eq!(photos.photo.get(0).unwrap().id, "photo_1_id");
            assert_eq!(photos.photo.get(0).unwrap().owner, "photo_1_owner");
            assert_eq!(photos.photo.get(0).unwrap().secret, "photo_1_secret");
            assert_eq!(photos.photo.get(0).unwrap().server, "photo_1_server");
            assert_eq!(photos.photo.get(0).unwrap().farm, 4);
            assert_eq!(photos.photo.get(0).unwrap().title, "photo_1_title");
            assert_eq!(photos.photo.get(0).unwrap().ispublic, true);
            assert_eq!(photos.photo.get(0).unwrap().isfriend, true);
            assert_eq!(photos.photo.get(0).unwrap().isfamily, true);
            assert_eq!(photos.photo.get(1).unwrap().id, "photo_2_id");
            assert_eq!(photos.photo.get(1).unwrap().ispublic, false);
            assert_eq!(photos.photo.get(1).unwrap().isfriend, false);
            assert_eq!(photos.photo.get(1).unwrap().isfamily, false);
        }
    }

    #[test]
    fn get_perms() {
        let mut flickr = setup(r#"
            {
              "perms": {
                "id": "photo_id",
                "ispublic": 1,
                "isfriend": 1,
                "isfamily": 1,
                "permcomment": 3,
                "permaddmeta": 2
              },
              "stat": "ok"
            }
        "#);
 
        let r = flickr
                    .photos()
                    .get_perms()
                    .photo_id("12345678")
                    .perform()
                    .unwrap();
        assert_eq!(r.stat, "ok");
        if let Some(perms) = r.perms {
            assert_eq!(perms.id, "photo_id");
            assert_eq!(perms.ispublic, true);
            assert_eq!(perms.isfriend, true);
            assert_eq!(perms.isfamily, true);
            assert_eq!(perms.permcomment, 3);
            assert_eq!(perms.permaddmeta, 2);
        }
    }

    #[test]
    fn get_popular() {
        let mut flickr = setup(r#"
            {
              "photos": {
                "page": 1,
                "pages": 1,
                "perpage": 100,
                "total": 76,
                "photo": [
                  {
                    "id": "photo_1_id",
                    "owner": "photo_1_owner",
                    "secret": "photo_1_secret",
                    "server": "photo_1_server",
                    "farm": 1,
                    "title": "photo_1_title",
                    "ispublic": 1,
                    "isfriend": 1,
                    "isfamily": 1
                  },
                  {
                    "id": "photo_2_id",
                    "owner": "photo_2_owner",
                    "secret": "photo_2_secret",
                    "server": "photo_2_server",
                    "farm": 2,
                    "title": "photo_2_title",
                    "ispublic": 0,
                    "isfriend": 0,
                    "isfamily": 0
                  }
                ]
              },
              "stat": "ok"
            }
        "#);
 
        let r = flickr
                    .photos()
                    .get_popular()
                    .perform()
                    .unwrap();
        assert_eq!(r.stat, "ok");
        if let Some(photos) = r.photos {
            assert_eq!(photos.page, 1);
            assert_eq!(photos.pages, 1);
            assert_eq!(photos.perpage, 100);
            assert_eq!(photos.total, 76);
            assert_eq!(photos.photo.get(0).unwrap().id, "photo_1_id");
            assert_eq!(photos.photo.get(0).unwrap().owner, "photo_1_owner");
            assert_eq!(photos.photo.get(0).unwrap().secret, "photo_1_secret");
            assert_eq!(photos.photo.get(0).unwrap().server, "photo_1_server");
            assert_eq!(photos.photo.get(0).unwrap().farm, 1);
            assert_eq!(photos.photo.get(0).unwrap().title, "photo_1_title");
            assert_eq!(photos.photo.get(0).unwrap().ispublic, true);
            assert_eq!(photos.photo.get(0).unwrap().isfriend, true);
            assert_eq!(photos.photo.get(0).unwrap().isfamily, true);
            assert_eq!(photos.photo.get(1).unwrap().id, "photo_2_id");
            assert_eq!(photos.photo.get(1).unwrap().ispublic, false);
            assert_eq!(photos.photo.get(1).unwrap().isfriend, false);
            assert_eq!(photos.photo.get(1).unwrap().isfamily, false);
        }
    }

    #[test]
    fn get_sizes() {
        let mut flickr = setup(r#"
            {
                "sizes": {
                    "canblog": 1,
                    "canprint": 1,
                    "candownload": 1,
                    "size": [
                        {
                            "label": "Square",
                            "width": 75,
                            "height": 75,
                            "source": "https://123456",
                            "url": "https://7890",
                            "media": "photo"
                        },
                        {
                            "label": "Square",
                            "width": 75,
                            "height": 75,
                            "source": "https://1234567",
                            "url": "https://78901",
                            "media": "photo"
                        }
                    ]
                },
                "stat": "ok"
            }
        "#);
 
        let r = flickr
                    .photos()
                    .get_sizes()
                    .perform()
                    .unwrap();
        assert_eq!(r.stat, "ok");
        if let Some(sizes) = r.sizes {
            assert_eq!(sizes.canblog, true);
            assert_eq!(sizes.canprint, true);
            assert_eq!(sizes.candownload, true);
            assert_eq!(sizes.size.get(0).unwrap().label, "Square");
            assert_eq!(sizes.size.get(0).unwrap().width, 75);
            assert_eq!(sizes.size.get(0).unwrap().height, 75);
            assert_eq!(sizes.size.get(0).unwrap().source, "https://123456");
            assert_eq!(sizes.size.get(0).unwrap().url, "https://7890");
            assert_eq!(sizes.size.get(0).unwrap().media, "photo");
            assert_eq!(sizes.size.get(1).unwrap().label, "Square");
            assert_eq!(sizes.size.get(1).unwrap().width, 75);
            assert_eq!(sizes.size.get(1).unwrap().height, 75);
            assert_eq!(sizes.size.get(1).unwrap().source, "https://1234567");
            assert_eq!(sizes.size.get(1).unwrap().url, "https://78901");
            assert_eq!(sizes.size.get(1).unwrap().media, "photo");
        }
    }

    #[test]
    fn get_untagged() {
        let mut flickr = setup(r#"
            {
              "photos": {
                "page": 1,
                "pages": 1,
                "perpage": 100,
                "total": "35",
                "photo": [
                  {
                    "id": "photo_1_id",
                    "owner": "photo_1_owner",
                    "secret": "photo_1_secret",
                    "server": "photo_1_server",
                    "farm": 4,
                    "title": "photo_1_title",
                    "ispublic": 1,
                    "isfriend": 1,
                    "isfamily": 1
                  },
                  {
                    "id": "photo_2_id",
                    "owner": "photo_2_owner",
                    "secret": "photo_2_secret",
                    "server": "photo_2_server",
                    "farm": 4,
                    "title": "photo_2_title",
                    "ispublic": 0,
                    "isfriend": 0,
                    "isfamily": 0
                  }
                ]
              },
              "stat": "ok"
            }
        "#);
 
        let r = flickr
                    .photos()
                    .get_untagged()
                    .perform()
                    .unwrap();
        assert_eq!(r.stat, "ok");
        if let Some(photos) = r.photos {
            assert_eq!(photos.page, 1);
            assert_eq!(photos.pages, 1);
            assert_eq!(photos.perpage, 100);
            assert_eq!(photos.total, 35);
            assert_eq!(photos.photo.get(0).unwrap().id, "photo_1_id");
            assert_eq!(photos.photo.get(0).unwrap().owner, "photo_1_owner");
            assert_eq!(photos.photo.get(0).unwrap().secret, "photo_1_secret");
            assert_eq!(photos.photo.get(0).unwrap().server, "photo_1_server");
            assert_eq!(photos.photo.get(0).unwrap().farm, 4);
            assert_eq!(photos.photo.get(0).unwrap().title, "photo_1_title");
            assert_eq!(photos.photo.get(0).unwrap().ispublic, true);
            assert_eq!(photos.photo.get(0).unwrap().isfriend, true);
            assert_eq!(photos.photo.get(0).unwrap().isfamily, true);
            assert_eq!(photos.photo.get(1).unwrap().id, "photo_2_id");
            assert_eq!(photos.photo.get(1).unwrap().ispublic, false);
            assert_eq!(photos.photo.get(1).unwrap().isfriend, false);
            assert_eq!(photos.photo.get(1).unwrap().isfamily, false);
        }
    }
    
    #[test]
    fn get_with_geo_data_builder() {
        let mut flickr = setup(r#"
            {
              "photos": {
                "page": 1,
                "pages": 1,
                "perpage": 100,
                "total": "35",
                "photo": [
                  {
                    "id": "photo_1_id",
                    "owner": "photo_1_owner",
                    "secret": "photo_1_secret",
                    "server": "photo_1_server",
                    "farm": 4,
                    "title": "photo_1_title",
                    "ispublic": 1,
                    "isfriend": 1,
                    "isfamily": 1
                  },
                  {
                    "id": "photo_2_id",
                    "owner": "photo_2_owner",
                    "secret": "photo_2_secret",
                    "server": "photo_2_server",
                    "farm": 4,
                    "title": "photo_2_title",
                    "ispublic": 0,
                    "isfriend": 0,
                    "isfamily": 0
                  }
                ]
              },
              "stat": "ok"
            }
        "#);
 
        let r = flickr
                    .photos()
                    .get_with_geo_data_builder()
                    .perform()
                    .unwrap();
        assert_eq!(r.stat, "ok");
        if let Some(photos) = r.photos {
            assert_eq!(photos.page, 1);
            assert_eq!(photos.pages, 1);
            assert_eq!(photos.perpage, 100);
            assert_eq!(photos.total, 35);
            assert_eq!(photos.photo.get(0).unwrap().id, "photo_1_id");
            assert_eq!(photos.photo.get(0).unwrap().owner, "photo_1_owner");
            assert_eq!(photos.photo.get(0).unwrap().secret, "photo_1_secret");
            assert_eq!(photos.photo.get(0).unwrap().server, "photo_1_server");
            assert_eq!(photos.photo.get(0).unwrap().farm, 4);
            assert_eq!(photos.photo.get(0).unwrap().title, "photo_1_title");
            assert_eq!(photos.photo.get(0).unwrap().ispublic, true);
            assert_eq!(photos.photo.get(0).unwrap().isfriend, true);
            assert_eq!(photos.photo.get(0).unwrap().isfamily, true);
            assert_eq!(photos.photo.get(1).unwrap().id, "photo_2_id");
            assert_eq!(photos.photo.get(1).unwrap().ispublic, false);
            assert_eq!(photos.photo.get(1).unwrap().isfriend, false);
            assert_eq!(photos.photo.get(1).unwrap().isfamily, false);
        }
    }
    
    #[test]
    fn get_without_geo_data_builder() {
        let mut flickr = setup(r#"
            {
              "photos": {
                "page": 1,
                "pages": 1,
                "perpage": 100,
                "total": "35",
                "photo": [
                  {
                    "id": "photo_1_id",
                    "owner": "photo_1_owner",
                    "secret": "photo_1_secret",
                    "server": "photo_1_server",
                    "farm": 4,
                    "title": "photo_1_title",
                    "ispublic": 1,
                    "isfriend": 1,
                    "isfamily": 1
                  },
                  {
                    "id": "photo_2_id",
                    "owner": "photo_2_owner",
                    "secret": "photo_2_secret",
                    "server": "photo_2_server",
                    "farm": 4,
                    "title": "photo_2_title",
                    "ispublic": 0,
                    "isfriend": 0,
                    "isfamily": 0
                  }
                ]
              },
              "stat": "ok"
            }
        "#);
 
        let r = flickr
                    .photos()
                    .get_without_geo_data_builder()
                    .perform()
                    .unwrap();
        assert_eq!(r.stat, "ok");
        if let Some(photos) = r.photos {
            assert_eq!(photos.page, 1);
            assert_eq!(photos.pages, 1);
            assert_eq!(photos.perpage, 100);
            assert_eq!(photos.total, 35);
            assert_eq!(photos.photo.get(0).unwrap().id, "photo_1_id");
            assert_eq!(photos.photo.get(0).unwrap().owner, "photo_1_owner");
            assert_eq!(photos.photo.get(0).unwrap().secret, "photo_1_secret");
            assert_eq!(photos.photo.get(0).unwrap().server, "photo_1_server");
            assert_eq!(photos.photo.get(0).unwrap().farm, 4);
            assert_eq!(photos.photo.get(0).unwrap().title, "photo_1_title");
            assert_eq!(photos.photo.get(0).unwrap().ispublic, true);
            assert_eq!(photos.photo.get(0).unwrap().isfriend, true);
            assert_eq!(photos.photo.get(0).unwrap().isfamily, true);
            assert_eq!(photos.photo.get(1).unwrap().id, "photo_2_id");
            assert_eq!(photos.photo.get(1).unwrap().ispublic, false);
            assert_eq!(photos.photo.get(1).unwrap().isfriend, false);
            assert_eq!(photos.photo.get(1).unwrap().isfamily, false);
        }
    }

    #[test]
    fn recently_updated_builder() {
        let mut flickr = setup(r#"
            {
              "photos": {
                "page": 1,
                "pages": 1,
                "perpage": 100,
                "total": "35",
                "photo": [
                  {
                    "id": "photo_1_id",
                    "owner": "photo_1_owner",
                    "secret": "photo_1_secret",
                    "server": "photo_1_server",
                    "farm": 4,
                    "title": "photo_1_title",
                    "ispublic": 1,
                    "isfriend": 1,
                    "isfamily": 1
                  },
                  {
                    "id": "photo_2_id",
                    "owner": "photo_2_owner",
                    "secret": "photo_2_secret",
                    "server": "photo_2_server",
                    "farm": 4,
                    "title": "photo_2_title",
                    "ispublic": 0,
                    "isfriend": 0,
                    "isfamily": 0
                  }
                ]
              },
              "stat": "ok"
            }
        "#);
 
        let r = flickr
                    .photos()
                    .recently_updated_builder()
                    .perform()
                    .unwrap();
        assert_eq!(r.stat, "ok");
        if let Some(photos) = r.photos {
            assert_eq!(photos.page, 1);
            assert_eq!(photos.pages, 1);
            assert_eq!(photos.perpage, 100);
            assert_eq!(photos.total, 35);
            assert_eq!(photos.photo.get(0).unwrap().id, "photo_1_id");
            assert_eq!(photos.photo.get(0).unwrap().owner, "photo_1_owner");
            assert_eq!(photos.photo.get(0).unwrap().secret, "photo_1_secret");
            assert_eq!(photos.photo.get(0).unwrap().server, "photo_1_server");
            assert_eq!(photos.photo.get(0).unwrap().farm, 4);
            assert_eq!(photos.photo.get(0).unwrap().title, "photo_1_title");
            assert_eq!(photos.photo.get(0).unwrap().ispublic, true);
            assert_eq!(photos.photo.get(0).unwrap().isfriend, true);
            assert_eq!(photos.photo.get(0).unwrap().isfamily, true);
            assert_eq!(photos.photo.get(1).unwrap().id, "photo_2_id");
            assert_eq!(photos.photo.get(1).unwrap().ispublic, false);
            assert_eq!(photos.photo.get(1).unwrap().isfriend, false);
            assert_eq!(photos.photo.get(1).unwrap().isfamily, false);
        }
    }

    #[test]
    fn remove_tag() {
        let mut flickr = setup(r#"
            {"stat":"ok"}
        "#);
 
        let r = flickr.photos()
                      .remove_tag()
                      .photo_id("123456789")
                      .tag_id("aa")
                      .perform()
                      .unwrap();
        assert_eq!(r.stat, "ok");
    }

    #[test]
    fn set_content_type() {
        let mut flickr = setup(r#"
            {"stat":"ok"}
        "#);
 
        let r = flickr.photos()
                      .set_content_type()
                      .photo_id("123456789")
                      .content_type("aa")
                      .perform()
                      .unwrap();
        assert_eq!(r.stat, "ok");
    }

    #[test]
    fn set_dates() {
        let mut flickr = setup(r#"
            {"stat":"ok"}
        "#);
 
        let r = flickr.photos()
                      .set_dates()
                      .photo_id("123456789")
                      .date_posted(Local.ymd(2018, 11, 26).and_hms(20, 32, 4))
                      .date_taken(Local.ymd(2018, 11, 26).and_hms(20, 32, 4))
                      .date_taken_granularity(8)
                      .perform()
                      .unwrap();
        assert_eq!(r.stat, "ok");
    }

    #[test]
    fn set_meta() {
        let mut flickr = setup(r#"
            {"stat":"ok"}
        "#);
 
        let r = flickr.photos()
                      .set_meta()
                      .photo_id("123456789")
                      .title("title")
                      .description("description")
                      .perform()
                      .unwrap();
        assert_eq!(r.stat, "ok");
    }

    #[test]
    fn set_perms() {
        let mut flickr = setup(r#"
            {"stat":"ok"}
        "#);
 
        let r = flickr.photos()
                      .set_perms()
                      .photo_id("123456789")
                      .is_public(true)
                      .is_friend(true)
                      .is_family(true)
                      .perm_comment(0)
                      .perm_addmeta(0)
                      .perform()
                      .unwrap();
        assert_eq!(r.stat, "ok");
    }

    #[test]
    fn set_safety_level() {
        let mut flickr = setup(r#"
            {"stat":"ok"}
        "#);
 
        let r = flickr.photos()
                      .set_safety_level()
                      .photo_id("123456789")
                      .safety_level(1)
                      .hidden(true)
                      .perform()
                      .unwrap();
        assert_eq!(r.stat, "ok");
    }

    #[test]
    fn set_tags() {
        let mut flickr = setup(r#"
            {"stat":"ok"}
        "#);
 
        let r = flickr.photos()
                      .set_tags()
                      .photo_id("123456789")
                      .tags("aa,bb,cc")
                      .perform()
                      .unwrap();
        assert_eq!(r.stat, "ok");
    }
}

