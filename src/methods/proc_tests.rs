
use flickr_derive::{Builder};
use super::super::{FlickrError};

#[derive(Builder)]
#[method = "boat.search"]
#[result = "BoatQueryResult"]
struct BoatQuery<'a> {
      flickr: &'a mut DummyFlickr,
      kind: &'a str,
      class: Option<&'a str>,
      loa_min: Option<f64>,
      loa_max: Option<f64>,
      crew_min: i32,
      crew_max: i32,
}

#[derive(Deserialize)]
struct BoatQueryResult {
    method: String,
    kind: String,
    class: String,
}

struct DummyFlickr {
}

impl DummyFlickr {
    pub fn is_authenticated(&self) -> bool {
        true
    }
    pub fn do_auth(&self) -> Result<(), FlickrError> {
        Ok(())
    }
    pub fn call_method(&self, method: &str, params: std::collections::BTreeMap<&str, String>) -> Result<(String), FlickrError> {
        let s = format!("{{ \"method\": \"{}\", \"kind\": \"{}\", \"class\": \"{}\" }}", 
                        method, 
                        {
                            if params.contains_key("kind") {
                                params["kind"].clone()
                            } else {
                                "".into()
                            }
                        },
                        {
                            if params.contains_key("class") {
                                params["class"].clone()
                            } else {
                                "".into()
                            }
                        },
                        );
        Ok(s.into())
    }
}

#[test]
fn test_derive_builder() {
    let mut flickr = DummyFlickr{};
    let mut boat = BoatQuery::new(&mut flickr);

    boat
        .kind("sailing")
        .class("Ilur")
        .loa_min(4.00)
        .loa_max(5.00)
        .crew_min(1)
        .crew_max(5);

    assert_eq!(boat.kind, "sailing");
    assert_eq!(boat.class, Some("Ilur"));
    assert_eq!(boat.loa_min, Some(4.00));
    assert_eq!(boat.loa_max, Some(5.00));
    assert_eq!(boat.crew_min, 1);
    assert_eq!(boat.crew_max, 5);
    
    match boat.perform() {
        Ok(result) => {
            assert_eq!(result.method, "boat.search");
            assert_eq!(result.kind, "sailing");
            assert_eq!(result.class, "Ilur");
        },
        Err(e) => {
            assert_eq!("ok", e.to_string());
        }
    }
}

